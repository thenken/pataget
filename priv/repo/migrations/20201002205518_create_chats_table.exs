defmodule Pataget.Repo.Migrations.CreateChatsTable do
  use Ecto.Migration

  def change do
    create table "chats" do
      add :text, :text
      add :time, :date
      add :user_id, references("users")

      timestamps()
    end
  end
end
