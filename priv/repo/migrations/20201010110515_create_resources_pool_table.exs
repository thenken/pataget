defmodule Pataget.Repo.Migrations.CreateResourcesPoolTable do
  use Ecto.Migration

  def change do
    create table(:resources_pool) do
      add :amount, :integer
      add :user_id, references(:users)
      add :project_uuid, references(:projects, type: :uuid, column: :project_uuid) 
      timestamps()
    end
  end
end
