defmodule Pataget.Repo.Migrations.CreateRequests do
  use Ecto.Migration

  def change do
    create table(:requests) do
      add :requested_by, :string
      add :amount, :integer
      add :interest, :decimal
      add :payment_date, :date

      timestamps()
    end

  end
end
