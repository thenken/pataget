defmodule Pataget.Repo.Migrations.AlterRequestsTable do
  use Ecto.Migration

  def change do
    alter table "requests" do
      add :user_rating, :integer
    end
  end
end
