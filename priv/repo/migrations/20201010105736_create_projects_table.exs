defmodule Pataget.Repo.Migrations.CreateProjectsTable do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")

    create table(:projects, primary_key: false) do
      add :project_uuid, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :target_amount, :integer
      add :expected_returns, :integer
      timestamps()
    end

    create index(:projects, [:project_uuid])
  end
end
