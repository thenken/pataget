defmodule Pataget.Repo.Migrations.AlterUsersTable do
  use Ecto.Migration

  def change do
    alter table "users" do
      add :roles, :string
      add :avatar, :text
      add :avatar_name, :string
      add :rating, :integer
    end
  end
end
