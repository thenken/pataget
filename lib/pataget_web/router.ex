defmodule PatagetWeb.Router do
  use PatagetWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug PatagetWeb.Auth.Pipeline
  end

  scope "/api", PatagetWeb do
    pipe_through :api
    post "/users/signup", UserController, :create
    post "/users/signin", UserController, :signin
    post "/users/upload", UserController, :upload
  end

  scope "/api", PatagetWeb do
    pipe_through [:api, :auth]
    resources "/requests", LoanRequestsController, except: [:new, :edit]
  end
end
