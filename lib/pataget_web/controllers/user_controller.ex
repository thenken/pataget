defmodule PatagetWeb.UserController do
  use PatagetWeb, :controller

  alias Pataget.Accounts
  alias Pataget.Repo
  alias Pataget.Accounts.User
  alias PatagetWeb.Auth.Guardian

  action_fallback PatagetWeb.FallbackController

  def create(conn, user_params) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params),
    {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  def upload(conn, %{"email" => email} = params) do
    user = Repo.get_by(User, email: email)

    with {:ok, %User{} = user} <- Accounts.update_user(user, params) do
      conn
      |> put_status(:created)
      |> render("update_user.json", %{user: user})
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    with {:ok, user, token} <- Guardian.authenticate(email, password) do
      conn
      |> put_status(:created)
      |> render("user.json", %{user: user, token: token})
    end
  end

  # access the current user 
  # current_user = Guardian.Plug.current_resource(conn)

  # revoke a token for the purpose of logout
  # {:ok, claims} = BusiApiWeb.Auth.Guardian.revoke(token)
end
