defmodule PatagetWeb.LoanRequestsController do
  use PatagetWeb, :controller

  alias Pataget.Finance
  alias Pataget.Finance.LoanRequests

  action_fallback PatagetWeb.FallbackController

  def index(conn, _params) do
    requests = Finance.list_requests()
    render(conn, "index.json", requests: requests)
  end

  def create(conn, %{"payment_date" => date} = params) do
    params = %{params | "payment_date" => Date.from_iso8601!(date)}

    with {:ok, %LoanRequests{} = loan_requests} <- Finance.create_loan_requests(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.loan_requests_path(conn, :show, loan_requests))
      |> render("show.json", loan_requests: loan_requests)
    end
  end

  def show(conn, %{"id" => id}) do
    loan_requests = Finance.get_loan_requests!(id)
    render(conn, "show.json", loan_requests: loan_requests)
  end

  def update(conn, %{"id" => id, "loan_requests" => loan_requests_params}) do
    loan_requests = Finance.get_loan_requests!(id)

    with {:ok, %LoanRequests{} = loan_requests} <- Finance.update_loan_requests(loan_requests, loan_requests_params) do
      render(conn, "show.json", loan_requests: loan_requests)
    end
  end

  def delete(conn, %{"id" => id}) do
    loan_requests = Finance.get_loan_requests!(id)

    with {:ok, %LoanRequests{}} <- Finance.delete_loan_requests(loan_requests) do
      send_resp(conn, :no_content, "")
    end
  end
end
