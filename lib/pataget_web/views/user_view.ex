defmodule PatagetWeb.UserView do
  use PatagetWeb, :view
  alias PatagetWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user, token: token}) do
    %{
      email: user.email,
      username: user.username,
      roles: user.roles,
      avatar: user.avatar,
      token: token
    }
  end

  def render("update_user.json", %{user: user}) do
    %{
      email: user.email,
      username: user.username,
      avatar: user.avatar,
      avatar_name: user.avatar_name,
      roles: user.roles
    }
  end
end
