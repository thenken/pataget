defmodule PatagetWeb.LoanRequestsView do
  use PatagetWeb, :view
  alias PatagetWeb.LoanRequestsView

  def render("index.json", %{requests: requests}) do
    %{data: render_many(requests, LoanRequestsView, "loan_requests.json")}
  end

  def render("show.json", %{loan_requests: loan_requests}) do
    %{data: render_one(loan_requests, LoanRequestsView, "loan_requests.json")}
  end

  def render("loan_requests.json", %{loan_requests: loan_requests}) do
    %{post_date: Timex.format!(loan_requests.inserted_at, "{D} {Mshort} {YYYY}"),
      requested_by: loan_requests.requested_by,
      user_rating: loan_requests.user_rating,
      amount: loan_requests.amount,
      interest: loan_requests.interest,
      payment_date: Timex.format!(loan_requests.payment_date, "{D} {Mshort} {YYYY}")
    }
  end
end
