defmodule PatagetWeb.Auth.Pipeline do
    use Guardian.Plug.Pipeline, 
        otp_app: :pataget_api,
        module: PatagetWeb.Auth.Guardian,
        error_handler: PatagetWeb.Auth.ErrorHandler
  
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
  end