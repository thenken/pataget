defmodule Pataget.Finance do
  @moduledoc """
  The Finance context.
  """

  import Ecto.Query, warn: false
  alias Pataget.Repo

  alias Pataget.Finance.LoanRequests

  @doc """
  Returns the list of requests.

  ## Examples

      iex> list_requests()
      [%LoanRequests{}, ...]

  """
  def list_requests do
    from(l in LoanRequests, order_by: [desc: l.inserted_at])
    |> Repo.all()
  end

  @doc """
  Gets a single loan_requests.

  Raises `Ecto.NoResultsError` if the Loan requests does not exist.

  ## Examples

      iex> get_loan_requests!(123)
      %LoanRequests{}

      iex> get_loan_requests!(456)
      ** (Ecto.NoResultsError)

  """
  def get_loan_requests!(id), do: Repo.get!(LoanRequests, id)

  @doc """
  Creates a loan_requests.

  ## Examples

      iex> create_loan_requests(%{field: value})
      {:ok, %LoanRequests{}}

      iex> create_loan_requests(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_loan_requests(attrs \\ %{}) do
    %LoanRequests{}
    |> LoanRequests.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a loan_requests.

  ## Examples

      iex> update_loan_requests(loan_requests, %{field: new_value})
      {:ok, %LoanRequests{}}

      iex> update_loan_requests(loan_requests, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_loan_requests(%LoanRequests{} = loan_requests, attrs) do
    loan_requests
    |> LoanRequests.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a loan_requests.

  ## Examples

      iex> delete_loan_requests(loan_requests)
      {:ok, %LoanRequests{}}

      iex> delete_loan_requests(loan_requests)
      {:error, %Ecto.Changeset{}}

  """
  def delete_loan_requests(%LoanRequests{} = loan_requests) do
    Repo.delete(loan_requests)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking loan_requests changes.

  ## Examples

      iex> change_loan_requests(loan_requests)
      %Ecto.Changeset{source: %LoanRequests{}}

  """
  def change_loan_requests(%LoanRequests{} = loan_requests) do
    LoanRequests.changeset(loan_requests, %{})
  end
end
