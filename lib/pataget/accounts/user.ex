defmodule Pataget.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :username, :string
    field :password, :string, virtual: true
    field :encrypted_password, :string
    field :roles, :string
    field :avatar, :string
    field :avatar_name, :string
    field :rating, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :username, :password, :roles, :avatar, :avatar_name, :rating])
    |> validate_required([:email, :username])
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/)
    |> unique_constraint(:username)
    |> validate_length(:password, min: 6)
    |> put_hashed_password
  end

  defp put_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}}
        ->
          put_change(changeset, :encrypted_password, Comeonin.Bcrypt.hashpwsalt(password))
          |> IO.inspect(label: "encrypted_password  --")
      _ ->
          changeset
    end
  end
end
