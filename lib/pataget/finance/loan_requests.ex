defmodule Pataget.Finance.LoanRequests do
  use Ecto.Schema
  import Ecto.Changeset

  schema "requests" do
    field :amount, :integer
    field :interest, :decimal
    field :payment_date, :date
    field :requested_by, :string
    field :user_rating, :integer

    timestamps()
  end

  @doc false
  def changeset(loan_requests, attrs) do
    loan_requests
    |> cast(attrs, [:requested_by, :amount, :interest, :payment_date, :user_rating])
    |> validate_required([:requested_by, :amount, :interest, :payment_date])
  end
end
