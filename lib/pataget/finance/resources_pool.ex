defmodule Pataget.ResourcesPool do
    use Ecto.Schema
    import Ecto.Changeset
    alias Pataget.Repo

    # @foreign_key_type Ecto.UUID
    schema "resources_pool" do
        field :amount, :integer
        belongs_to :user, Pataget.Accounts.User
        belongs_to :project, Pataget.Project, references: :project_uuid, type: Ecto.UUID, foreign_key: :project_uuid
        timestamps()   
    end

    def changeset(struct \\ %__MODULE__{}, params) do
        cast(struct, params, [:amount, :user_id, :project_uuid]) 
    end

    def enter(params) do
        changeset(params)
        |> Repo.insert
    end
end