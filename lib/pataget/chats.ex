
  defmodule Pataget.Accounts.Chats do
    use Ecto.Schema
    import Ecto.Changeset
    alias Pataget.Accounts.User
    alias Pataget.Repo

    schema "chats" do
      field :text, :string
      field :time, :date
      belongs_to :user, User

      timestamps()
    end

  def changeset(struct \\ %__MODULE__{}, params) do
    struct
    |> cast(params, [:text, :time, :user_id])
  end

  def add_chat(params) do
    params
    |> changeset()
    |> Repo.insert()
  end

end