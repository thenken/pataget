defmodule Pataget.Repo do
  use Ecto.Repo,
    otp_app: :pataget,
    adapter: Ecto.Adapters.Postgres
end
