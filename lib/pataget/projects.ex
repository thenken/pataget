defmodule Pataget.Project do
    use Ecto.Schema
    import Ecto.Changeset
    alias Pataget.Repo

    @primary_key {:project_uuid, Ecto.UUID, autogenerate: false}
    schema "projects" do
        field :target_amount, :integer
        field :expected_returns, :integer    
        has_many :resources_pool, Pataget.ResourcesPool, foreign_key: :project_uuid
        timestamps()
    end

    def changeset(struct \\ %__MODULE__{}, params) do
        cast(struct, params, [:target_amount, :expected_returns]) 
    end

    def create(params) do
        changeset(params)
        |> Repo.insert
    end
end