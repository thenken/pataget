use Mix.Config

# Configure your database
config :pataget, Pataget.Repo,
  username: "postgres",
  password: "postgres",
  database: "pataget_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :pataget, PatagetWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
