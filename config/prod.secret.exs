# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

config :pataget, Pataget.Repo,
  username: "postgres",
  password: "postgres",
  database: "pataget_prod",
  pool_size: 15

config :automzero, PatagetWeb.Endpoint,
  secret_key_base: "88ZWPKtFsJjzoj1n8VdbWF0t8TLF/lZzidHi/1G8S7qViEky4t+ItF00MI/rZ1l3",
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4070")],
  server: true

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :pataget, PatagetWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
