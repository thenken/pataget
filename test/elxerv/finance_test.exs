defmodule Pataget.FinanceTest do
  use Pataget.DataCase

  alias Pataget.Finance

  describe "requests" do
    alias Pataget.Finance.LoanRequests

    @valid_attrs %{amount: 42, interest: "120.5", payment_date: ~D[2010-04-17], requested_by: "some requested_by"}
    @update_attrs %{amount: 43, interest: "456.7", payment_date: ~D[2011-05-18], requested_by: "some updated requested_by"}
    @invalid_attrs %{amount: nil, interest: nil, payment_date: nil, requested_by: nil}

    def loan_requests_fixture(attrs \\ %{}) do
      {:ok, loan_requests} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Finance.create_loan_requests()

      loan_requests
    end

    test "list_requests/0 returns all requests" do
      loan_requests = loan_requests_fixture()
      assert Finance.list_requests() == [loan_requests]
    end

    test "get_loan_requests!/1 returns the loan_requests with given id" do
      loan_requests = loan_requests_fixture()
      assert Finance.get_loan_requests!(loan_requests.id) == loan_requests
    end

    test "create_loan_requests/1 with valid data creates a loan_requests" do
      assert {:ok, %LoanRequests{} = loan_requests} = Finance.create_loan_requests(@valid_attrs)
      assert loan_requests.amount == 42
      assert loan_requests.interest == Decimal.new("120.5")
      assert loan_requests.payment_date == ~D[2010-04-17]
      assert loan_requests.requested_by == "some requested_by"
    end

    test "create_loan_requests/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Finance.create_loan_requests(@invalid_attrs)
    end

    test "update_loan_requests/2 with valid data updates the loan_requests" do
      loan_requests = loan_requests_fixture()
      assert {:ok, %LoanRequests{} = loan_requests} = Finance.update_loan_requests(loan_requests, @update_attrs)
      assert loan_requests.amount == 43
      assert loan_requests.interest == Decimal.new("456.7")
      assert loan_requests.payment_date == ~D[2011-05-18]
      assert loan_requests.requested_by == "some updated requested_by"
    end

    test "update_loan_requests/2 with invalid data returns error changeset" do
      loan_requests = loan_requests_fixture()
      assert {:error, %Ecto.Changeset{}} = Finance.update_loan_requests(loan_requests, @invalid_attrs)
      assert loan_requests == Finance.get_loan_requests!(loan_requests.id)
    end

    test "delete_loan_requests/1 deletes the loan_requests" do
      loan_requests = loan_requests_fixture()
      assert {:ok, %LoanRequests{}} = Finance.delete_loan_requests(loan_requests)
      assert_raise Ecto.NoResultsError, fn -> Finance.get_loan_requests!(loan_requests.id) end
    end

    test "change_loan_requests/1 returns a loan_requests changeset" do
      loan_requests = loan_requests_fixture()
      assert %Ecto.Changeset{} = Finance.change_loan_requests(loan_requests)
    end
  end
end
