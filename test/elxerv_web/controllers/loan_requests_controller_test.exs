defmodule PatagetWeb.LoanRequestsControllerTest do
  use PatagetWeb.ConnCase

  alias Pataget.Finance
  alias Pataget.Finance.LoanRequests

  @create_attrs %{
    amount: 42,
    interest: "120.5",
    payment_date: ~D[2010-04-17],
    requested_by: "some requested_by"
  }
  @update_attrs %{
    amount: 43,
    interest: "456.7",
    payment_date: ~D[2011-05-18],
    requested_by: "some updated requested_by"
  }
  @invalid_attrs %{amount: nil, interest: nil, payment_date: nil, requested_by: nil}

  def fixture(:loan_requests) do
    {:ok, loan_requests} = Finance.create_loan_requests(@create_attrs)
    loan_requests
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all requests", %{conn: conn} do
      conn = get(conn, Routes.loan_requests_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create loan_requests" do
    test "renders loan_requests when data is valid", %{conn: conn} do
      conn = post(conn, Routes.loan_requests_path(conn, :create), loan_requests: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.loan_requests_path(conn, :show, id))

      assert %{
               "id" => id,
               "amount" => 42,
               "interest" => "120.5",
               "payment_date" => "2010-04-17",
               "requested_by" => "some requested_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.loan_requests_path(conn, :create), loan_requests: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update loan_requests" do
    setup [:create_loan_requests]

    test "renders loan_requests when data is valid", %{conn: conn, loan_requests: %LoanRequests{id: id} = loan_requests} do
      conn = put(conn, Routes.loan_requests_path(conn, :update, loan_requests), loan_requests: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.loan_requests_path(conn, :show, id))

      assert %{
               "id" => id,
               "amount" => 43,
               "interest" => "456.7",
               "payment_date" => "2011-05-18",
               "requested_by" => "some updated requested_by"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, loan_requests: loan_requests} do
      conn = put(conn, Routes.loan_requests_path(conn, :update, loan_requests), loan_requests: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete loan_requests" do
    setup [:create_loan_requests]

    test "deletes chosen loan_requests", %{conn: conn, loan_requests: loan_requests} do
      conn = delete(conn, Routes.loan_requests_path(conn, :delete, loan_requests))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.loan_requests_path(conn, :show, loan_requests))
      end
    end
  end

  defp create_loan_requests(_) do
    loan_requests = fixture(:loan_requests)
    {:ok, loan_requests: loan_requests}
  end
end
